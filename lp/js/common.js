/*---------------------------------------------
	function
  ---------------------------------------------*/

$(function(){
  //viewport
    if($(window).width()>=769){
      $('head').prepend('<meta name="viewport" content="width=device-width, maximum-scale=1">');
    } else {
      $('head').prepend('<meta name="viewport" content="target-densitydpi=device-dpi, width=1020, maximum-scale=1.0, user-scalable=yes">');
    }

  //スクロール
  $('a[href^="#"]').on('click', function(){
    var speed = 600;
		var fixedheight = $('header').innerHeight();
    var href= $(this).attr("href");
    var target = $(href == "#" || href == "" ? 'html' : href);
    var position = target.offset().top - fixedheight;
    $("html, body").animate({scrollTop:position}, speed, "swing");
    return false;
  });

	});


/*---------------------------------------------
	電話番号
  ---------------------------------------------*/

var ua = navigator.userAgent.toLowerCase();
var isMobile = /iphone/.test(ua)||/android(.+)?mobile/.test(ua);

if (!isMobile) {
    $('a[href^="tel:"]').on('click', function(e) {
        e.preventDefault();
    });
}