<?php /* Template Name: 他社採用イベントとの違い */ ?>
<?php get_header(); ?>

<?php get_template_part('part-page_header'); ?>


<article class="l-content_wrapper">
  <section class="c-alt_container">
    <div class="c-alt_container-title" data-emergence="hidden">
      <h2>
        <em class="c-alt_container-title--eng">採用イベントの大別</em>
        <em class="c-alt_container-title--ja">General classification</em>
      </h2>
    </div>
    <div class="c-alt_container-content">
      <h3 class="c-alt_container-tagline" data-emergence="hidden">
        足切り型
      </h3>
      <div class="c-alt_container-description" data-emergence="hidden">
        <div class="p-classification-type-image">
          <img src="/images/image-speciality-type--ashikiri.svg" alt="足切り型">
        </div><!-- /.p-classification-type-image -->
        <div class="p-classification-type-text">
          短時間で大人数と接触。いい子に目星をつけて後日連絡するタイプの採用イベント。（合説や説明会型イベント。ワーク型イベントなど）
          <ul class="pure">
            <li>学生がそもそも行きたがっているネームバリューのある会社は安いリード単価で多くの学生に出会えるため効果的です。</li>
            <li>BtoB企業やベンチャー企業など認知度に課題がある企業にとっての効果性は薄いと言えます。</li>
          </ul>
        </div><!-- /.p-classification-type-text -->
      </div><!-- /.c-alt_container-description -->
    </div><!-- /.c-alt_container-content -->
  </section>

  <section class="c-alt_container">
    <div class="c-alt_container-content">
      <h3 class="c-alt_container-tagline" data-emergence="hidden">
        訴求型
      </h3>
      <div class="c-alt_container-description" data-emergence="hidden">
        <div class="p-classification-type-text">
          長時間かけて少数精鋭としっかり関係性構築できるタイプのイベントです。自社の魅力を伝え、学生の魅力をしっかりと把握できるイベントになります。
          <ul class="pure">
            <li>BtoB企業やベンチャー企業など認知度に課題がある企業にとっての効果性は非常に高く、魅力づけがしっかりとできるため、高い流入数を期待できます。</li>
            <li>インバウンド集客がうまくできる企業にとっては工数が多くかかり非効率です。</li>
          </ul>
        </div><!-- /.p-classification-type-text -->
        <div class="p-classification-type-image">
          <img src="/images/image-speciality-type--sokyu.svg" alt="訴求型">
        </div><!-- /.p-classification-type-image -->
        <div class="p-extra_content">
          <hr>
          <p>Growth Stage【T】は足切り型<br>Growth Stage【D】【P】は訴求型となります。<br>
          <p>企業様の採用課題に合わせた柔軟で幅広い活用が可能です。</p>
        </div>
      </div><!-- /.c-alt_container-description -->
    </div><!-- /.c-alt_container-content -->
  </section>

  <section class="c-alt_container--blue">
    <div class="c-alt_container-title" data-emergence="hidden">
      <h2>
        <em class="c-alt_container-title--eng">就職意向</em>
        <em class="c-alt_container-title--ja">Job hunting intention</em>
      </h2>
    </div>
    <div class="c-alt_container-content">
      <h3 class="c-alt_container-tagline" data-emergence="hidden">
        インターンシップ参加企業への就職意向と、就職したいと思った社数／平均
      </h3>
      <div class="p-job_hunting_intention-content">
        <table class="p-data_table tbl_list striped bordered">
          <thead>
            <tr>
              <th></th>
              <th>全体</th>
              <th>文系男子</th>
              <th>文系女子</th>
              <th>理系男子</th>
              <th>理系女子</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th>就職したいと思う企業があった</th>
              <td>72.9%</td>
              <td>75.4%</td>
              <td>70.9%</td>
              <td>74.3%</td>
              <td>70.0%</td>
            </tr>
            <tr>
              <th>就職したいと思う企業はなかった</th>
              <td>27.1%</td>
              <td>24.6%</td>
              <td>29.1%</td>
              <td>25.7%</td>
              <td>30.0%</td>
            </tr>
            <tr>
              <td colspan="6"></td>
            </tr>
            <tr>
              <th>インターンシップ参加社数（平均）</th>
              <td>3.3社</td>
              <td>3.6社</td>
              <td>3.5社</td>
              <td>2.9社</td>
              <td>2.7社</td>
            </tr>
            <tr>
              <th>就職したいと思った社数（平均）</th>
              <td>1.2社</td>
              <td>1.3社</td>
              <td>1.1社</td>
              <td>1.2社</td>
              <td>1.1社</td>
            </tr>
          </tbody>
        </table>
        <p class="p-data_table-notice">
          ※「インターンシップ参加者数（平均）」は、日数にかかわらず参加経験を持つ人が分母<br>
          ※「就職したいと思った社数（平均）」は、就職したいと思う企業が０社と回答した人も含む
        </p>
        <p class="fs-h4 p-recommend_text">Growth Stage Seriesはインターン顔負けの本気度でお送りします。<br>過去参加企業、過去参加学生、運営メンバー全員で行うからこそ、体験価値の高い場となります。</p>
      </div><!-- /.c-alt_container-description -->
    </div><!-- /.c-alt_container-content -->
  </section>
</article><!-- /.l-content_wrapper -->

<?php get_template_part('part-page_parallel_links'); ?>

<?php get_footer(); ?>
