<?php
// ###########################################################
//
// 「次のページへ」及び「前のページへ」にクラス付与
//
// ###########################################################
add_filter('next_posts_link_attributes', 'next_link_attributes');
add_filter('previous_posts_link_attributes', 'previous_link_attributes');
function next_link_attributes(){
  return 'class="next"';
}
function previous_link_attributes(){
  return 'class="previous"';
}
?>
