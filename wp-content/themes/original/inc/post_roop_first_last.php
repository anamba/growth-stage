<?php
// ###########################################################
//
// 記事ループ　最初と最後の判定
//
// ###########################################################
function isFirst(){
    global $wp_query;
    return ($wp_query->current_post === 0);
}

function isLast(){
    global $wp_query;
    return ($wp_query->current_post+1 === $wp_query->post_count);
}
?>
