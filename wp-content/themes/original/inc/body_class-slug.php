<?php
// ###########################################################
//
// Page slug to body_class()
//
// ###########################################################
function pagename_class($classes = '') {
    if (is_page()) {
        $page = get_post(get_the_ID());
        $classes[] = 'page-' . $page->post_name;
        if ($page->post_parent) {
            $classes[] = 'page-' . get_page_uri($page->post_parent) . '-child';
       }
  }
  return $classes;
}
add_filter('body_class', 'pagename_class');
?>
