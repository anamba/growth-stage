<?php
  // ###########################################################
  //
  // wordpress更新通知非表示
  //
  // ###########################################################
  add_action( 'admin_init', 'hide_update_msg', 1 );
  function hide_update_msg() {
      ! current_user_can( 'install_plugins' )
          and remove_action( 'admin_notices', 'update_nag', 3 );
  }
?>
