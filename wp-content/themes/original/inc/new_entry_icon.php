<?php
// ###########################################################
//
//  新着記事にアイコン
//  usage
//  $e_day = get_the_time('U');
//  echo entry_new_marking( $e_day );
//
// ###########################################################
function entry_new_marking($entry){
  $days = 14;
  $today = date_i18n('U');
  $sa = date('U',($today - $entry))/86400;
  if( $days > $sa ){
      $new_mark = '<strong class="new">NEW!</strong> ';
  } else {
      $new_mark = "";
  }
  return $new_mark;
}
add_theme_support( 'post-thumbnails');
?>
