<?php
// ###########################################################
//
// Navigation current state class
//
// ###########################################################
add_action('nav_menu_css_class', 'add_current_nav_class', 10, 2 );

function add_current_nav_class($classes, $item) {
  global $post;
  $id = ( isset( $post->ID ) ? get_the_ID() : NULL );
  if (isset( $id )){
    $current_post_type = get_post_type_object(get_post_type($post->ID));
    $current_post_type_slug = $current_post_type->rewrite['slug'];
    $menu_slug = strtolower(trim($item->url));
    if (strpos($menu_slug,$current_post_type_slug) !== false) {
      $classes[] = 'current-menu-item';
    }
  }
  return $classes;
}
?>
