<?php
// ###########################################################
//
// Page title
//
// ###########################################################
add_filter( 'document_title_separator', 'my_document_title_separator' );
function my_document_title_separator( $sep ) {
  $sep = '｜';
  return $sep;
}

function theme_slug_setup() {
   add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'theme_slug_setup' );
?>
