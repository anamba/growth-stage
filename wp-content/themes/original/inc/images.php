<?php
// ###########################################################
//
// 画像のサイズ
//
// ###########################################################
function disable_calculate_image_srcset($sources) {
  return false;
}
add_filter('wp_calculate_image_srcset', 'disable_calculate_image_srcset');
function thumbnail_size_setup() {
  if(update_option('thumbnail_size_w', 320)) {
    update_option('thumbnail_size_h', 180);
  }
}
function medium_size_setup() {
  if(update_option('medium_size_w', 720)) {
    update_option('medium_size_h', 405);
  }
}
function large_size_setup() {
  if(update_option('large_size_w', 1280)) {
    update_option('large_size_h', 720);
  }
}
add_action('after_setup_theme', 'thumbnail_size_setup');
add_action('after_setup_theme', 'medium_size_setup');
add_action('after_setup_theme', 'large_size_setup');


// ###########################################################
//
// サムネイルの圧縮率
//
// ###########################################################
function jpeg_quality_callback($arg) {
    return (int)95;
}
add_filter('jpeg_quality', 'jpeg_quality_callback');


// ###########################################################
//
// 画像出力時の付加属性調整
//
// ###########################################################
function remove_img_attributes( $html ) {
$html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
return $html;
}
add_filter( 'post_thumbnail_html', 'remove_img_attributes', 10 );
add_filter( 'image_send_to_editor', 'remove_img_attributes', 10 );



// ###########################################################
//
// SVGアップロード許可
//
// ###########################################################
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');
?>
