<?php /* Template Name: 参加企業の声 */ ?>
<?php get_header(); ?>

<?php get_template_part('part-page_header'); ?>


<article class="l-content_wrapper">
  <?php if( have_rows('company_voices') ): while( have_rows('company_voices') ): the_row(); ?>
    <section class="c-alt_container">
      <div class="c-alt_container-title" data-emergence="hidden">
        <h2>
          <em class="c-alt_container-title--eng"><?php echo get_sub_field('company_name'); ?></em>
          <em class="c-alt_container-title--ja"><?php echo get_sub_field('employee_name'); ?></em>
        </h2>
      </div>
      <div class="c-alt_container-content">
        <h3 class="c-alt_container-tagline" data-emergence="hidden">
          <?php echo get_sub_field('company_container-tagline'); ?>
        </h3>
        <div class="c-alt_container-description">
          <dl class="p-companies_voice-questions_list">
            <?php if( have_rows('questions') ): while( have_rows('questions') ): the_row(); ?>
              <dt data-emergence="hidden"><?php echo get_sub_field('question-query'); ?></dt>
              <dd data-emergence="hidden"><?php echo get_sub_field('question-answer'); ?></dd>
            <?php endwhile; endif; ?>
          </dl>
        </div>
        <div class="c-alt_container-image" data-emergence="hidden" style="background-image: url(<?php echo wp_get_attachment_image_url( get_sub_field('company_container-image'), 'full' ); ?>);">
        </div>
      </div>
    </section>
  <?php endwhile; endif; ?>

  <section class="c-wide_container--blue">
    <div class="c-wide_container-title">
      <h2>
        <span class="c-wide_container-title--eng">Companies</span>
        <span class="c-wide_container-title--ja">参加企業</span>
      </h2>
    </div>
    <div class="c-wide_container-content_wrapper">
      <div class="c-wide_container-content">
        <?php if( have_rows('company_logos', 2) ): ?>
          <ul class="c-logo_list">
            <?php while( have_rows('company_logos', 2) ): the_row(); ?>
              <li><?php $company_logo_array =  get_sub_field('company_logo'); $company_logo = $company_logo_array['sizes']['thumbnail']; ?>
                <img src="<?php echo $company_logo; ?>" alt="<?php echo $company_logo_array['alt']; ?>">
              </li>
            <?php endwhile; ?>
          </ul>
        <?php endif; ?>

        <?php if( have_rows('companies_name_list') ): ?>
          <ul class="p-companies_name_list">
            <?php while( have_rows('companies_name_list') ): the_row(); ?>
              <li><?php echo get_sub_field('company_name'); ?></li>
            <?php endwhile; ?>
          </ul>
        <?php endif; ?>
      </div>
    </div>
  </section>

  <div class="l-content_inwrapper">
  </div><!-- /.l-content_inwrapper -->
</article><!-- /.l-content_wrapper -->

<?php get_template_part('part-page_parallel_links'); ?>

<?php get_footer(); ?>
