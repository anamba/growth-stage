<!DOCTYPE html>
<html lang="ja" xmlns:og="https://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
<meta name="format-detection" content="telephone=yes">

<link rel="shortcut icon" href="/images/favicon.ico" />
<link rel="alternate" type="application/rss xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />

<!-- CSS -->
<link rel="stylesheet" href="/css/style.css?20180124-a">

<!-- Scripts -->
<script src="/js/rellax.min.js"></script>
<script src="/js/jquery-1.11.3.min.js"></script>
<script src="/js/superembed.min.js"></script>
<script src="/js/headroom.min.js"></script>
<script src="/js/jquery.headroom.min.js"></script>
<script src="/js/emergence.min.js"></script>
<!--[if lt IE 10]>
<script src="/js/flexie.min.js"></script>
<![endif]-->
<script src="/js/common.min.js?20171211-a"></script>

<?php wp_head(); ?>

</head>
<body id="pagetop" <?php body_class(); ?>>

  <header id="header" class="headroom l-header">
    <div class="l-header-inwrapper">
      <span class="c-nav_global-sp_toggle"><img src="/images/icon-menu.svg" alt="MENU"></span>
      <strong class="l-header-logo"><a href="<?php echo home_url(); ?>" title="<?php wp_title(); ?>"><img src="http://growth-stage2018.com/wp-content/uploads/2018/02/GS_logo.png" alt="Growth Stage"></a></strong>
      <h1 class="l-header-seotext">学生と企業が互いに真撃に向き合う為のマッチングサービス</h1>

      <nav>
        <ul class="c-nav_global">
          <li class="c-nav_global-sp_toggle--close"><img src="/images/icon-close_sp_nav.svg" alt="Close Menu"></li>
          <li><a href="/unique_point" title="イベントの特徴">イベントの特徴</a></li>
          <li><a href="/speciality" title="他社採用イベントとの違い">他社採用イベントとの違い</a></li>
          <li><a href="/event_categories" title="イベントの種類">イベントの種類</a></li>
          <li><a href="/students" title="参画している学生">参画している学生</a></li>
          <li><a href="/companies" title="参加企業の声">参加企業の声</a></li>
          <li><a href="/company_information" title="企業情報">企業情報</a></li>
          <li class="c-nav_global-contact"><a href="/contact" title="お問い合わせ">お問い合わせ（無料）はこちら<i class="fa fa-angle-right"></i></a></li>
        </ul>
      </nav>
    </div><!-- /.l-header-inwrapper -->
  </header>
