<?php
error_reporting(0);
add_editor_style('/css/editor-style.css');
add_theme_support( 'post-thumbnails');

remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'rel_canonical');

register_nav_menu('footernav', 'フッターナビゲーション');
register_nav_menu('globalnav', 'グローバルナビゲーション');

add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1);
add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1);
add_filter('page_css_class', 'my_css_attributes_filter', 100, 1);

function my_css_attributes_filter($var) {
  return is_array($var) ? array_intersect($var, array('current-menu-item')) : '';
}

add_action( 'admin_enqueue_scripts', 'enqueue_admin_style_script' );
function enqueue_admin_style_script() {
  //wp_enqueue_style( 'admin-style', get_template_directory_uri() . '/css/admin_style.css' );
  //wp_enqueue_script( 'admin-script', get_template_directory_uri() . '/js/admin__script.js', array(), '1.0.0', true );
}

require get_parent_theme_file_path( '/inc/hide_update_msg.php' );

require get_parent_theme_file_path( '/inc/nav_current_class.php' );

require get_parent_theme_file_path( '/inc/excerpt_more.php' );

require get_parent_theme_file_path( '/inc/excerpt_length.php' );

require get_parent_theme_file_path( '/inc/body_class-slug.php' );

require get_parent_theme_file_path( '/inc/images.php' );

require get_parent_theme_file_path( '/inc/hide_category_tab.php' );

require get_parent_theme_file_path( '/inc/disable_emoji.php' );

require get_parent_theme_file_path( '/inc/new_entry_icon.php' );

require get_parent_theme_file_path( '/inc/no_link_term_list.php' );

require get_parent_theme_file_path( '/inc/post_roop_first_last.php' );

require get_parent_theme_file_path( '/inc/parallel_link_class.php' );

require get_parent_theme_file_path( '/inc/page_title.php' );

require get_parent_theme_file_path( '/inc/wpcf7.php' );

?>
