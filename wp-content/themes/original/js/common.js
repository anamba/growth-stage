//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//
//  Header height set to .mainvisual
//
//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//;(function() {
//    'use strict';
//    var root = this;
//    root.navigationController = function() {
//      var $navToggle = $('.nav_toggle'),
//          $toggleIcon = $('.nav_toggle .icon'),
//          $navWrapper = $('.nav_global'),
//          showClass = 'nav_show';
//
//      $navToggle.on('click',function(){
//        $navWrapper.toggleClass(showClass);
//        $navToggle.toggleClass(showClass);
//        $toggleIcon.toggleClass(showClass);
//        $('body').toggleClass(showClass);
//      });
//    };
//}).call(this);
//
//$(document).ready(function() {
//  navigationController();
//});

//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//
//  Smartphone Navigation
//
//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
$(document).ready(function() {
  var $navToggle = $('.l-header').find('.c-nav_global-sp_toggle'),
      $navClose = $('.c-nav_global').find('.c-nav_global-sp_toggle--close');

  $navToggle.click(function(){
    $('.c-nav_global').removeClass('blurOut');
    $('.c-nav_global').addClass('c-nav_global-sp--show cssanimation blurIn');
    $('body').css({'overflow-y': 'hidden'});
  });
  $navClose.click(function(){
    $('.c-nav_global').removeClass('c-nav_global-sp--show blurIn');
    $('.c-nav_global').addClass('blurOut');
    $('body').css({'overflow-y': 'scroll'});
  });
});


//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//
//  Responsive table
//
//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
;(function() {
    'use strict';
    var root = this;
    root.satoriUiResponsiveTableFunc = function() {
        $('.tbl_responsive table').unwrap();
        $('.wysiwyg_contents table').wrap('<div class="tbl_responsive"></div>');
    };
}).call(this);

$(document).ready(function() {
  satoriUiResponsiveTableFunc();
});


//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//
//  Scroll up
//
//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
$(document).ready(function() {
  var $scrUp = $('.p-heroheader_scroll');
  $(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
      $scrUp.removeClass('cssanimation');
      $scrUp.addClass('onScroll');
    } else {
      $scrUp.addClass('cssanimation');
      $scrUp.removeClass('onScroll');
    }
  });
});


//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//
//  Headroom
//
//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
$(function() {
  $("#header").headroom({
    "offset": 10,
    "tolerance": 5,
    "classes": {
      initial: "cssanimation",
      pinned: "moveFromTop",
      unpinned: "moveToTop"
    }
  });
});

// ###########################################################
//
// Rellax
//
// ###########################################################
$(document).ready(function() {
	var rellax = new Rellax('.rellax');
});


// ###########################################################
//
// Scroll Add Class
//
// ###########################################################
(function($) {
	$.fn.sac = function(options) {

		var elements = this;
		var defaults = {
      screenPos: 0.7
		};
		var setting = $.extend(defaults, options);

    $(window).scroll(function (){
      elements.each(function(){
        var className = $(this).data('sac-class');
        var winScroll = $(window).scrollTop();
        var winHeight = $(window).height();
        var scrollPos = winScroll + (winHeight * setting.screenPos);

        if($(this).offset().top < scrollPos) {
          $(this).addClass(className);
        }
        //else {
        //  $(this).removeClass(className);
        //}
      });
    });
	}
})(jQuery);

$(document).ready(function() {
  $('.sac').sac();
});


// ###########################################################
//
// emergence.js
//
// ###########################################################
;(function() {
    'use strict';
    var root = this;
    root.emergenceInitFunc = function() {

      //var emergenceContainer = document.querySelector('.emergence');

      emergence.init({
        //container: emergenceContainer,
        container: window,
        reset: false,
        handheld: true,
        throttle: 250,
        elemCushion: 0.15, // Default 0.15
        offsetTop: 0,
        offsetRight: 0,
        offsetBottom: 0,
        offsetLeft: 0,
        callback: function(element, state) {
          if (state === 'visible') {
            //console.log('Element is visible.');
          } else if (state === 'reset') {
            //console.log('Element is hidden with reset.');
          } else if (state === 'noreset') {
            //console.log('Element is hidden with NO reset.');
          }
        }
      });
    };
}).call(this);

$(document).ready(function() {
  emergenceInitFunc();
});
