<?php get_header(); ?>
<?php if(have_posts()): the_post(); ?>
<?php get_template_part('part-mainvisual'); ?>


<article>
  <section class="section_gutter block_layer2_alt">
    <div class="inwrapper">

      <?php get_template_part('part-sns'); ?>

      <section class="wysiwyg_contents">
        <?php the_content(); ?>
      </section><!-- /.wysiwyg_contents -->

      <?php get_template_part('part-post_parallel_links'); ?>
      <?php get_template_part('part-sns'); ?>
      <?php get_template_part('part-cta_alt'); ?>

    </div><!-- /.inwrapper -->
  </section>

  <?php get_template_part('part-cta'); ?>
</article>

<?php endif; wp_reset_query(); ?>
<?php get_footer(); ?>