<section class="c-page_parallel_link">
  <div class="c-page_parallel_link-inwrapper">
    <ul>
      <?php
        $parallel_link_pageIds = array(83, 101, 118, 121, 123);
        foreach ($parallel_link_pageIds as $tmp):
      ?>
      <li>
        <a href="<?php the_permalink($tmp); ?>" title="<?php the_title($tmp); ?>">
          <div class="c-page_parallel_link-image">
            <img src="<?php echo wp_get_attachment_image_url( get_field('page-header_image', $tmp), 'thumbnail'); ?>" alt="">
          </div><!-- /.c-page_parallel_link-image -->
          <h1 class="c-page_parallel_link-title--eng"><?php echo get_field('page-eng_name', $tmp); ?></h1>
          <span class="c-page_parallel_link-title--ja"><?php echo get_the_title($tmp); ?></span>
        </a>
      </li>
    <?php endforeach; ?>
    </ul>
  </div><!-- /.c-page_parallel_links-inwrapper -->
</section><!-- /.c-page_parallel_links -->
