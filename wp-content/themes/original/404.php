<?php /* Template Name: 404ページ */ ?>
<?php get_header(); ?>

<article class="l-content_wrapper">
  <div class="l-content_inwrapper">
    <h2 class="section_title">404 PAGE NOT FOUND<br>ご指定のページが見つかりません</h2>
    <p class="centered">申し訳ございません。このページは現在アクセスできません。</p>
    <p class="centered">お手数ですが、トップページより目的のページをお探しください。</p>
    <p class="centered"><a href="/" title="トップページへ">トップページへ</a></p>
  </div><!-- /.l-content_inwrapper -->
</article><!-- /.l-content_wrapper -->

<?php get_template_part('part-page_parallel_links'); ?>

<?php get_footer(); ?>
