<?php /* Template Name: 企業情報 */ ?>
<?php get_header(); ?>

<?php get_template_part('part-page_header'); ?>


<article class="l-content_wrapper">
  <div class="l-content_inwrapper">

    <section class="c-wide_container--transparent">
      <div class="c-wide_container-title">
        <h2>
          <span class="c-wide_container-title--eng">Company profile</span>
          <span class="c-wide_container-title--ja">会社概要</span>
        </h2>
      </div>
      <div class="c-wide_container-content_wrapper p-company_information">
        <dl>
          <?php if( have_rows('company_informations') ): ?>
            <?php while( have_rows('company_informations') ): the_row(); ?>
              <dt><?php echo get_sub_field('information-head'); ?></dt>
              <dd><?php echo get_sub_field('information-content'); ?></dd>
            <?php endwhile; ?>
          <?php endif; ?>
        </dl>
      </div>
    </section>

  </div><!-- /.l-content_inwrapper -->

  <section class="c-wide_container--blue p-message_from_ceo">
    <div class="c-wide_container-title">
      <h2>
        <span class="c-wide_container-title--eng">Message from CEO</span>
        <span class="c-wide_container-title--ja">代表からのメッセージ</span>
      </h2>
    </div>
    <div class="c-wide_container-content_wrapper" style="background-image: url(<?php echo wp_get_attachment_image_url( get_field('page-header_image', 151), 'medium' ); ?>);">
      <div class="c-wide_container-content">

      </div>
      <h3 class="c-wide_container-tagline"><?php echo get_field('ceo-catch_copy', 151); ?></h3>
      <a href="company_information/messagefromceo" class="c-wide_container-link">代表からのメッセージへ</a>
    </div>
  </section>

  <section class="c-wide_container--yellow p-access_map">
    <div class="c-wide_container-title">
      <h2>
        <span class="c-wide_container-title--eng">Access</span>
        <span class="c-wide_container-title--ja">アクセス</span>
      </h2>
    </div>
    <div class="c-wide_container-content_wrapper">
      <div class="c-wide_container-content">
        <div id="map" style="height: 600px;"></div>
      </div>
    </div>
  </section>
</article><!-- /.l-content_wrapper -->



<script>
  function initMap() {
    var mapLatlng = new google.maps.LatLng(35.661386,139.714502);

    var mapstyle = [
      {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#444444"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#f2f2f2"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "hue": "#00ff1c"
            },
            {
                "visibility": "on"
            },
            {
                "gamma": "1.69"
            },
            {
                "lightness": "10"
            },
            {
                "saturation": "-28"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 45
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#c0dae4"
            },
            {
                "visibility": "on"
            }
        ]
    },
      {
        featureType:"all",
        elementType:"all",
        stylers: [
          {visibility: "on" }
        ]
      }
    ];

    var myOptions = {
      zoom: 17,
      center: mapLatlng,
      mapTypeIds: [
        'mainmap',
        google.maps.MapTypeId.HYBRID,
        google.maps.MapTypeId.SATELLITE,
        google.maps.MapTypeId.TERRAIN
      ]
    };
    var map = new google.maps.Map(document.getElementById("map"), myOptions);

    var styleMapOptions = {
      map: map,
      name:"アクセス"
    }
    var myMapType = new google.maps.StyledMapType(mapstyle, styleMapOptions);
    map.mapTypes.set('mainmap', myMapType);
    map.setMapTypeId('mainmap');

    var marker = new google.maps.Marker({
        position: mapLatlng
    });

    // To add the marker to the map, call setMap();
    marker.setMap(map)
  }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCxUJRvcDUK4vr6SfvClHfzcZOYnmxPqbw&callback=initMap" async defer></script>
<?php get_template_part('part-page_parallel_links'); ?>

<?php get_footer(); ?>
