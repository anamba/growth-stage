
<div class="c-page_header rellax" style="background-image: url('<?php echo wp_get_attachment_image_url( get_field('page-header_image'), 'full' ); ?>')">
  <div class="c-page_header-inwrapper">
    <h1 class="c-page_header-page_title--ja"><?php the_title(); ?></h1>
    <strong class="c-page_header-page_title--eng"><?php echo get_field('page-eng_name'); ?></strong>
  </div><!-- -/.c-page_header-inwrapper -->
</div><!-- /.c-page_header -->
