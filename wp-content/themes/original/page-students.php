<?php /* Template Name: 参画している学生 */ ?>
<?php get_header(); ?>

<?php get_template_part('part-page_header'); ?>


<article class="l-content_wrapper">
  <section class="c-alt_container">
    <div class="c-alt_container-title" data-emergence="hidden">
      <h2>
        <em class="c-alt_container-title--eng">どんな学生が多いか</em>
        <em class="c-alt_container-title--ja">What kind of students are there?</em>
      </h2>
    </div>
    <div class="c-alt_container-content">
      <h3 class="c-alt_container-tagline" data-emergence="hidden">
        非・大手思考が80%以上
      </h3>
      <div class="c-alt_container-description u-wysiwyg_contents" data-emergence="hidden">
        <p>企業の知名度や経営基盤の安定よりも、挑戦していく姿勢を大切にしている学生を動員。</p>
		  <p>・ビジョンに共感できるか</p>
		  <p>・若手に裁量権があるか</p>
		  <p>・独自の技術や強みを持っているか</p>
		  <p>・経営者が魅力的であるかetc...</p>
        <p>意欲的な学生が参加することが特徴です。　※2019卒学生エントリー時のアンケートデータにて判断</p>
      </div>
      <div class="c-alt_container-image" data-emergence="hidden" style="background-image: url(http://growth-stage2018.com/wp-content/uploads/2018/02/S__10698773.jpg);"></div>
    </div>
  </section>

  <section class="c-alt_container--yellow">
    <div class="c-alt_container-content">
      <h3 class="c-alt_container-tagline" data-emergence="hidden">
        自身のキャリアに本気で向きあっている学生
      </h3>
      <div class="c-alt_container-description u-wysiwyg_contents" data-emergence="hidden">
        <p>・自分はどうなりたいのか??</p>
		  <p>・自分はどう在りたいのか??</p>
		  <p>・自分は何がしたいのか??</p>
        <p>社会人からのフィードバックを素直に受け止めることが出来るだけでなく、自分の中に落とし込んで考え抜くことが出来る学生を動員。本気で自分自身のキャリアに向き合っている学生が参加します。</p>
      </div>
      <div class="c-alt_container-image" data-emergence="hidden" style="background-image: url(http://growth-stage2018.com/wp-content/uploads/2018/02/S__10698772.jpg);"></div>
    </div>
  </section>

  <section class="c-alt_container--blue">
    <div class="c-alt_container-title" data-emergence="hidden">
      <h2>
        <em class="c-alt_container-title--eng">スクリーニング</em>
        <em class="c-alt_container-title--ja">Screening</em>
      </h2>
    </div>
    <div class="c-alt_container-content">
      <h3 class="c-alt_container-tagline" data-emergence="hidden">
        集え！No.1学生！
      </h3>
      <div class="c-alt_container-description u-wysiwyg_contents" data-emergence="hidden">
        分野等は一切問わず、人一倍努力し続けNo.1を勝ち取った経験のある学生を動員。<br>
        例）スポーツ大会にて優勝,特許を取得,起業実績,ビジネスコンテスト上位入賞,フロリダディズニーワールドにてベストダンスショー受賞etc...　※動員実績に基づき記載/エントリー者全員に対し事前スクリーニング面談を実施
      </div>
      <div class="c-alt_container-image" data-emergence="hidden" style="background-image: url(http://growth-stage2018.com/wp-content/uploads/2018/02/S__10698769.jpg);"></div>
    </div>
  </section>

  <section class="c-alt_container">
    <div class="c-alt_container-content">
      <h3 class="c-alt_container-tagline" data-emergence="hidden">
        人とは違う何かを！ユニーク学生！
      </h3>
      <div class="c-alt_container-description u-wysiwyg_contents" data-emergence="hidden">
        ごく普通の大学生活に物足りなさを感じ、幅広い価値観に触れるため日々行動していたユニーク学生を動員。<br>
        例）自転車で日本一周,ネパールにてスタディーツアー主催,,長期アメリカ留学,長期インターンシップにて商品企画・開発・リリース全て経験etc...　※動員実績に基づき記載/エントリー者全員に対し事前スクリーニング面談を実施
      </div>
      <div class="c-alt_container-image" data-emergence="hidden" style="background-image: url(http://growth-stage2018.com/wp-content/uploads/2018/02/S__10698770-1.jpg);"></div>
    </div>
  </section>

  <section class="c-wide_container--blue">
    <div class="c-wide_container-title">
      <h2>
        <span class="c-wide_container-title--eng">DATA</span>
        <span class="c-wide_container-title--ja">実績データ</span>
      </h2>
    </div>
    <div class="p-chart_list">
      <ul class="cols">
        <li class="col6" data-emergence="hidden">
          <img src="/images/image-chart_gakureki.png" alt="学歴">
        </li>
        <li class="col6" data-emergence="hidden">
          <img src="/images/image-chart_level.png" alt="レベル割合">
        </li>
        <li class="col6" data-emergence="hidden">
          <img src="/images/image-chart_exp.png" alt="インターン経験">
        </li>
        <li class="col6" data-emergence="hidden">
          <img src="/images/image-chart_num.png" alt="インターン経験社数">
        </li>
        <li class="col6" data-emergence="hidden">
          <img src="/images/image-chart_mind.png" alt="インターン企業規模の志向">
        </li>
        <li class="col6" data-emergence="hidden">
          <img src="/images/image-chart_sex.png" alt="男女比">
        </li>
        <li class="col6" data-emergence="hidden">
          <img src="/images/image-chart_type.png" alt="文理比">
        </li>
        <li class="col6" data-emergence="hidden">
          <img src="/images/image-chart_flow.png" alt="開催あたりの流入数">
        </li>
      </ul>
    </div>
  </section>

  </div><!-- /.l-content_inwrapper -->
</article><!-- /.l-content_wrapper -->

<?php get_template_part('part-page_parallel_links'); ?>

<?php get_footer(); ?>
