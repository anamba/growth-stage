<?php /* Template Name: 代表からのメッセージ */ ?>
<?php get_header(); ?>

<?php get_template_part('part-page_header--messagefromceo'); ?>


<article class="l-content_wrapper">
  <?php if( have_rows('ceo-message-sections') ): while( have_rows('ceo-message-sections') ): the_row(); ?>
    <section class="c-alt_container">
      <div class="c-alt_container-content">
        <h3 class="c-alt_container-tagline" data-emergence="hidden">
          <?php echo get_sub_field('ceo-message-head'); ?>
        </h3>
        <div class="c-alt_container-description" data-emergence="hidden">
          <?php echo get_sub_field('ceo-message-content'); ?>
        </div>
        <div class="c-alt_container-image" data-emergence="hidden" style="background-image: url(<?php echo wp_get_attachment_image_url( get_sub_field('ceo-message-image'), 'full' ); ?>);">
        </div>
      </div>
    </section>
  <?php endwhile; endif; ?>
</article><!-- /.l-content_wrapper -->

<?php get_template_part('part-page_parallel_links'); ?>

<?php get_footer(); ?>
