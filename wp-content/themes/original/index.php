<?php /* Template Name: トップページ */ ?>
<?php get_header(); ?>


<?php
  $hero_image = get_field('hero-image');
  $size = 'full';
?>
<div class="p-heroheader rellax" style="background-image: url('<?php echo wp_get_attachment_image_url( $hero_image, $size ); ?>')">
  <div class="p-heroheader-inwrapper">
    <img class="p-hero_tagline cssanimation blurIn rellax" data-rellax-speed="3" src="/images/text-hero_tagline.png" alt="学生と企業が互いに真撃に向き合う為のマッチングサービス">
  </div><!-- -/.p-heroheader-inwrapper -->
  <div class="p-heroheader_scroll">
    <img class="cssanimation hu__hu__ infinite" src="/images/icon-heroheader_scroll.png" alt="SCROLL">
  </div>
</div><!-- /.p-heroheader -->


<article class="l-content_wrapper">

  <?php if( have_rows('alt_containers') ): while( have_rows('alt_containers') ): the_row(); ?>
    <section class="c-alt_container">
      <div class="c-alt_container-title" data-emergence="hidden">
        <h2>
          <em class="c-alt_container-title--eng"><?php echo get_sub_field('alt_container-title--eng'); ?></em>
          <em class="c-alt_container-title--ja"><?php echo get_sub_field('alt_container-title--ja'); ?></em>
        </h2>
      </div>
      <div class="c-alt_container-content">
        <h3 class="c-alt_container-tagline" data-emergence="hidden">
          <?php echo get_sub_field('alt_container-tagline'); ?>
        </h3>
        <?php if( get_sub_field('alt_container-content') ): ?>
          <div class="c-alt_container-description u-wysiwyg_contents" data-emergence="hidden">
            <?php echo get_sub_field('alt_container-content'); ?>
          </div>
        <?php endif; ?>
        <?php if( get_sub_field('alt_container-link') ): ?>
          <a href="<?php echo get_sub_field('alt_container-link'); ?>" class="c-alt_container-link">詳しく見る<i class="fa fa-angle-right"></i></a>
        <?php endif; ?>
        <div class="c-alt_container-image" data-emergence="hidden" style="background-image: url(<?php echo wp_get_attachment_image_url( get_sub_field('alt_container-image'), 'full' ); ?>);">
        </div>
      </div>
    </section>
  <?php endwhile; endif; ?>

  <?php if( have_rows('wide_containers') ): while( have_rows('wide_containers') ): the_row(); ?>
    <?php $wide_container_color = get_sub_field('wide_container-color'); ?>
    <?php if( $wide_container_color == "yellow" ): ?>
      <section class="c-wide_container--yellow">
    <?php elseif( $wide_container_color == "blue" ): ?>
      <section class="c-wide_container--blue">
    <?php elseif( $wide_container_color == "transparent" ): ?>
      <section class="c-wide_container--transparent">
    <?php endif; ?>
      <div class="c-wide_container-title">
        <h2>
          <span class="c-wide_container-title--eng" data-emergence="hidden"><?php echo get_sub_field('wide_container-title--eng'); ?></span>
          <span class="c-wide_container-title--ja" data-emergence="hidden"><?php echo get_sub_field('wide_container-title--ja'); ?></span>
        </h2>
      </div>
      <div class="c-wide_container-content_wrapper" style="background-image: url(<?php echo wp_get_attachment_image_url( get_sub_field('wide_container-image'), 'full' ); ?>);">
        <?php if( get_sub_field('wide_container-content' ) ): ?>
          <div class="c-wide_container-content">
            <?php echo get_sub_field('wide_container-content'); ?>
          </div>
        <?php endif; ?>
        <?php if( get_sub_field('wide_container-tagline' ) ): ?>
          <h3 class="c-wide_container-tagline">
            <?php echo get_sub_field('wide_container-tagline'); ?>
          </h3>
        <?php endif; ?>
        <?php if( get_sub_field('wide_container-link') ): ?>
          <a href="<?php echo get_sub_field('wide_container-link'); ?>" class="c-wide_container-link">詳しく見る</a>
        <?php endif; ?>
      </div>
    </section>
  <?php endwhile; endif; ?>

  <section class="c-wide_container--transparent">
    <div class="c-wide_container-title">
      <h2>
        <span class="c-wide_container-title--eng" data-emergence="hidden">Companies voice</span>
        <span class="c-wide_container-title--ja" data-emergence="hidden">参加企業の声</span>
      </h2>
    </div>
    <div class="c-wide_container-content_wrapper">
      <div class="c-wide_container-content">
        <?php if( have_rows('company_logos') ): ?>
          <ul class="c-logo_list">
            <?php while( have_rows('company_logos') ): the_row(); ?>
              <li><?php $company_logo_array =  get_sub_field('company_logo'); $company_logo = $company_logo_array['sizes']['thumbnail']; ?>
                <img src="<?php echo $company_logo; ?>" alt="<?php echo $company_logo_array['alt']; ?>">
              </li>
            <?php endwhile; ?>
          </ul>
        <?php endif; ?>
      </div>
      <a href="/companies" class="c-wide_container-link" title="See more...">See more...</a>
    </div>
  </section>

  <section class="p-contact_container">
    <div class="p-contact_container-title">
      <h2>
        <span class="p-contact_container-title--eng">Contact</span>
        <span class="p-contact_container-title--ja">お問い合わせ</span>
      </h2>
    </div>
    <h3 class="p-contact_container-tagline">
      まずはお気軽にお問い合わせください。
    </h3>
    <a href="/contact" class="p-contact_container-link" title="Contact">Contact<i class="fa fa-angle-right"></i></a>
  </section>
</article><!-- /.p-content_wrapper -->


<?php get_footer(); ?>
