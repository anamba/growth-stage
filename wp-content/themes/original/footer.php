<footer id="footer">
  <span class="l-footer_copyright">@ 2017 キャリア支援団体SHOT</span>
	 <span class="l-footer_copyright">
	<p>
		<a href="http://growth-stage2018.com/privacypolicy_1">個人情報保護方針</a> / <a href="http://growth-stage2018.com/privacypolicy_2">個人情報の取り扱いについて</a> / <a href="http://growth-stage2018.com/privacypolicy_2">開示対象個人情報開示等請求書</a>
	</p>
	 </span>
</footer>

<?php wp_footer(); ?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-112912445-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-112912445-1');
</script>

<script src="/js/letteranimation.min.js"></script>
</body>
</html>
