<?php /* Template Name: イベントの特徴 */ ?>
<?php get_header(); ?>

<?php get_template_part('part-page_header'); ?>


<article class="l-content_wrapper">
  <div class="l-content_inwrapper">
    <?php if( have_rows('page-section_contents') ): ?>
      <section class="c-card_list p-card_list-override">
        <ul class="cols">
        <?php while( have_rows('page-section_contents') ): the_row(); ?>
          <li class="col6">
            <div class="c-card_list-inwrapper">
              <div class="c-card_list-image" data-emergence="hidden" style="background-image: url(<?php echo wp_get_attachment_image_url( get_sub_field('page-section_content-image'), 'medium' ); ?>);"></div>
              <div class="c-card_list-content_field" data-emergence="hidden">
                <h2 class="c-card_list-title"><?php echo get_sub_field('page-section_content-title'); ?></h2>
                <div class="c-card_list-content">
                  <?php echo get_sub_field('page-section_content-description'); ?>
                </div><!-- /.p-unique_point-content -->
              </div><!-- /.c-card_list-content_field -->
            </div><!-- /.inwrapper -->
          </li>
        <?php endwhile; ?>
        </ul>
      </section>
    <?php endif; ?>
  </div><!-- /.l-content_inwrapper -->
</article><!-- /.l-content_wrapper -->

<?php get_template_part('part-page_parallel_links'); ?>

<?php get_footer(); ?>
