
<div class="c-page_header rellax" style="background-image: url('<?php echo wp_get_attachment_image_url( get_field('page-header_image'), 'full' ); ?>')">
  <div class="c-page_header-inwrapper p-ceo_profile">
    <em class="p-ceo_profile-position">代表取締役</em>
    <h1 class="p-ceo_profile-name--ja"><?php echo get_field('ceo-name') ?></h1>
    <strong class="p-ceo_profile-name--eng"><?php echo get_field('ceo-name-eng'); ?></strong>
    <div class="p-ceo_profile-catchcopy">
      <?php echo get_field('ceo-catch_copy'); ?>
    </div><!-- /.p-ceo_profile-catchcopy -->
  </div><!-- -/.c-page_header-inwrapper -->
</div><!-- /.c-page_header -->
