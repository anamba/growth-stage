<?php /* Template Name: お問い合わせ */ ?>
<?php get_header(); ?>

<?php get_template_part('part-page_header'); ?>


<article class="l-content_wrapper">
  <div class="l-content_inwrapper">
    <?php $wpcf7_code = do_shortcode(get_field('cf7_code')); echo $wpcf7_code; ?>
  </div><!-- /.l-content_inwrapper -->
</article><!-- /.l-content_wrapper -->

<?php get_template_part('part-page_parallel_links'); ?>

<?php get_footer(); ?>
