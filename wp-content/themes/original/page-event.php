Growth Stage【Second】<?php /* Template Name: イベントの種類 */ ?>
<?php get_header(); ?>

<?php get_template_part('part-page_header'); ?>


<article class="l-content_wrapper">
  <div class="l-content_inwrapper">

    <section class="c-wide_container--transparent p-categories">
      <div class="c-wide_container-content_wrapper">
   <dl>
          <dt><span>短時間で多くの学生とマッチングを図りたい企業様は</br>Growth Stage【First】へ</span></dt>
          <dd><a href="#category_t" title="Growth Stage【First】概要"><img src="http://growth-stage2018.com/wp-content/uploads/2018/02/page-event-chart1-t.png" alt="Growth Stage【First】"></a></dd>
          <dt><span>中時間かけて中人数の優秀な学生との価値観マッチングを図りたい企業様は</br>Growth Stage【Second】へ</span></dt>
          <dd><a href="#category_d" title="Growth Stage【Second】概要"><img src="http://growth-stage2018.com/wp-content/uploads/2018/02/page-event-chart1-d.png" alt="Growth Stage【Second】"></a></dd>
          <dt><span>長時間かけて中人数の地方最高クラスの学生とのマッチングを図りたい企業様は</br>Growth Stage 【Third】へ</span></dt>
          <dd><a href="#category_p" title="Growth Stage【Third】概要"><img src="http://growth-stage2018.com/wp-content/uploads/2018/02/page-event-chart1-p.png" alt="Growth Stage【Third】"></a></dd>
          <dt><span>日本全国の最高クラスの学生に魅力づけをしたい企業様は</br>Growth Stage 【Final】へ</span></dt>
          <dd><a href="#category_top" title="Growth Stage【Top of Top】概要"><img src="http://growth-stage2018.com/wp-content/uploads/2018/02/page-event-chart1-top.png" alt="Growth Stage【Top of Top】"></a></dd>
    </dl>
      </div>
    </section>

  </div><!-- /.l-content_inwrapper -->

  <section id="category_t" class="c-alt_container--blue p-event_description">
    <div class="c-alt_container-title" data-emergence="hidden">
      <h2>
        <em class="c-alt_container-title--eng">Growth Stage 【First】</em>
      </h2>
    </div>
    <div class="c-alt_container-content">
      <div class="c-alt_container-description" data-emergence="hidden">
        <dl class="event_meta">
          <dt>参加人数:</dt><dd>40名前後</dd>
          <dt>参加企業様数:</dt><dd>4〜8社</dd>
          <dt>拘束時間:</dt><dd>約4時間</dd>
        </dl>
        <p>自分を変革したい学生がチーム（Team）となりひたすら試行錯誤（Try）を繰り返し<br>
        自分の人生やキャリアにおける鍛錬（Training）を通して<br>
        最高のチャンス生かす（Take a Chance）事を主眼としており、それぞれの頭文字である「T」をイベント名としております。</p>
        <p>約4時間で40人の学生とのポテンシャル・能力マッチングをすることが可能です。ワークを繰り返すことによって学生の学び取る力、吸収力を見極めることができるイベントとなっております。</p>
        <h3>ターゲット企業</h3>
        <ul>
          <li>短時間で多くの学生と接触したい企業様</li>
          <li>学生のポテンシャル（成長性）によって見極めをしたい企業様</li>
          <li>成長意欲の高い学生と出会いたい企業様</li>
        </ul>
        <h3>期待できる効果</h3>
        <ul>
          <li>インターンや自社イベントへの流入</li>
          <li>本選考への流入</li>
          <li>FBによる採用力の向上</li>
          <li>費用対効果、時間対効果の良い良質な学生への接触</li>
        </ul>
        <h3>参加学生のレベル感（ペルソナ）</h3>
        <p>人材系企業評価値 A:15% B:85%(参加段階)</p>
        <ul>
          <li>これから就活に力を入れようと思っている学生</li>
          <li>企業との出会いを求めている学生</li>
          <li>自分のやりたい事、なりたい姿を発見したい学生</li>
          <li>インターンを探している学生</li>
          <li>とにかく成長したいと考えている学生</li>
        </ul>
      </div><!-- /.c-alt_container-description -->

      <h3 class="c-alt_container-tagline" data-emergence="hidden">
        イベントのフロー
      </h3>
      <div class="event_flow-wrapper">
 

		    <ul class="event_flow">
          <li><span>事前面談動</span></li>
          <li><span>マインドセット</span></li>
          <li><span>企業プレゼン</span></li>
          <li><span>アイスブレイク</span></li>
          <li><span>ワーク①</span></li>
          <li><span>チーム変更①</span></li>
          <li><span>ワーク②</span></li>
          <li><span>チーム変更②</span></li>
          <li><span>ワーク③</span></li>
          <li><span>チーム変更③</span></li>
		  <li><span>テストGD</span></li>
          <li><span>懇親会（面談設定）</span></li>
		  <li><span>合否発表</span></li>
        </ul>
		  
		  
      </div><!-- /.event_flow-wrapper -->
    </div><!-- /.c-alt_container-content -->
  </section>

  <section id="category_d" class="c-alt_container--yellow p-event_description">
    <div class="c-alt_container-title" data-emergence="hidden">
      <h2>
        <em class="c-alt_container-title--eng">Growth Stage 【Second】</em>
      </h2>
    </div>
    <div class="c-alt_container-content">
      <div class="c-alt_container-description" data-emergence="hidden">
        <dl class="event_meta">
          <dt>参加人数:</dt><dd>20名前後</dd>
          <dt>参加企業様数:</dt><dd>3〜6社</dd>
          <dt>拘束時間:</dt><dd>約7時間</dd>
        </dl>
        <p>自分の理解を深める自己分析型のイベント。<br>
        自分を「深掘り（Dig）」「深め（Deepning）」「見出す（Discover）」<br>
        ことを主眼としており、それぞれの頭文字である「D」をイベント名としております。<br>
        企業様にとっては中人数の学生と一日で価値観マッチングをすることが可能です。Growth Staeg【Second】にて企業様からの評価、運営の査定をクリアした学生のみの参加となるので、当たり外れの少ない事も特徴です</p>
        <h3>ターゲット企業</h3>
        <ul>
          <li>価値観マッチングをしたい企業様</li>
          <li>能力ベースではなく理念共感型の採用をしたい企業様</li>
          <li>1日で価値観による振い落としをしたい企業様</li>
        </ul>
        <h3>期待できる効果</h3>
        <p>1日の価値観マッチングを通して、下記３点が期待できます。</p>
        <ul>
          <li>自社の魅力の認知向上</li>
          <li>インターンや自社イベントへの集客</li>
          <li>本選考への流入</li>
          <li>参加学生の心にまで踏み込む深く強い訴求が可能</li>
        </ul>
        <h3>参加学生のレベル感（ペルソナ）</h3>
        <p>人材系企業評価値 A:40% B:60</p>
        <ul>
          <li>能力やポテンシャル部分において企業様より評価の高い学生</li>
          <li>価値観という部分に置いて深掘りや将来の像がぼやけている学生</li>
          <li>自分に本気で向き合おうとしている学生</li>
          <li>成長意欲の高い人財</li>
        </ul>
      </div><!-- /.c-alt_container-description -->

      <h3 class="c-alt_container-tagline" data-emergence="hidden">
        イベントのフロー
      </h3>
      <div class="event_flow-wrapper">

        <ul class="event_flow">
          <li><span>事前面談</span></li>
          <li><span>マインドセット</span></li>
          <li><span>Happinessシート記入</span></li>
          <li><span>企業説明</span></li>
          <li><span>Happinessプレゼン/質疑応答、フィードバック</span></li>
          <li><span>ライフプラン枠組み提供</span></li>
          <li><span>再度考案プレゼン準備</span></li>
          <li><span>Happinessプレゼン/質疑応答、フィードバック②</span></li>
          <li><span>懇親会</span></li>
          <li><span>合否発表</span></li>
          <li><span>次回日程調整</span></li>
        </ul>
        <p class="subtext">※考え中カードを掲げている学生はFBを受けて考えを深堀っている学生です<br>
        ※同じ学生に何度も話を聞いて大丈夫です</p>
      </div>
    </div><!-- /.c-alt_container-content -->
  </section>

  <section id="category_p" class="c-alt_container--blue p-event_description">
    <div class="c-alt_container-title" data-emergence="hidden">
      <h2>
        <em class="c-alt_container-title--eng">Growth Stage 【Third】</em>
      </h2>
    </div>
    <div class="c-alt_container-content">
      <div class="c-alt_container-description" data-emergence="hidden">
        <dl class="event_meta">
          <dt>参加人数:</dt><dd>20〜24名前後（６チーム）</dd>
          <dt>参加企業様数:</dt><dd>5社</dd>
          <dt>拘束時間:</dt><dd>約3時間</dd>
        </dl>
        <p>Growth Stage【Third】において優勝したチームないしは参加したいチームが当日までに考案したビジネスアイデアを披露するビジネスコンテスト<br>
        日本全国のトップクラス学生に自社のブランディングや魅力づけができます。<br>
        個々人の採用やチームでの採用も実現します。</p>
        <h3>ターゲット企業</h3>
        <ul>
          <li><span>日本全国のトップクラス学生と出会いたい企業</span></li>
          <li><span>日本トップクラス学生に自社の認知度を高め、その周りの学生にも認知度を上げたい企業様</span></li>
        </ul>
        <h3>期待できる効果</h3>
        <ul>
          <li><span>トップクラス学生へのブランディング</span></li>
          <li><span>トップ層への自社の認知度の向上</span></li>
        </ul>
        <h3>参加学生のレベル感（ペルソナ）</h3>
        <p>企業に入社する学生の中でトップクラス<br>人材系企業評価値 A:70% B:30</p>
        <ul>
          <li><span>自分のやりたい事を理解し、能力、ポテンシャル共に申し分なく、ビジネスの素養もある学生。</span></li>
          <li><span>2年生の時から長期インターンや課外活動などをやっている学生</span></li>
          <li><span>最難関インターンに複数社参加している学生</span></li>
        </ul>
      </div><!-- /.c-alt_container-description -->

      <h3 class="c-alt_container-tagline" data-emergence="hidden">
        イベントのフロー
      </h3>
      <div class="event_flow-wrapper">
        <ul class="event_flow">
<li><span>企業様プレゼン</span></li>
<li><span>ワーク</span></li>
<li><span>ドラフトタイム</span></li>
<li><span>新規事業立案タイム</span></li>
<li><span>オファー制度</span></li>
<li><span>懇親会</span></li>
<li><span>合否発表</span></li>
<li><span>次回日程調整</span></li>
        </ul>
      </div>
    </div><!-- /.c-alt_container-content -->
  </section>

  <section id="category_top" class="c-alt_container--yellow p-event_description">
    <div class="c-alt_container-title" data-emergence="hidden">
      <h2>
        <em class="c-alt_container-title--eng">Growth Stage 【Final】</em>
      </h2>
    </div>
    <div class="c-alt_container-content">
      <div class="c-alt_container-description" data-emergence="hidden">
        <dl class="event_meta">
          <dt>参加人数:</dt><dd>20名前後</dd>
          <dt>参加企業様数:</dt><dd>3〜6社</dd>
          <dt>拘束時間:</dt><dd>約7時間</dd>
        </dl>
        <p>自分の理解を深める自己分析型のイベント。<br>
        自分を「深掘り（Dig）」「深め（Deepning）」「見出す（Discover）」<br>
        ことを主眼としており、それぞれの頭文字である「D」をイベント名としております。<br>
        企業様にとっては中人数の学生と一日で価値観マッチングをすることが可能です。Growth Staeg【Second】にて企業様からの評価、運営の査定をクリアした学生のみの参加となるので、当たり外れの少ない事も特徴です</p>
        <h3>ターゲット企業</h3>
        <ul>
          <li>価値観マッチングをしたい企業様</li>
          <li>能力ベースではなく理念共感型の採用をしたい企業様</li>
          <li>1日で価値観による振い落としをしたい企業様</li>
        </ul>
        <h3>期待できる効果</h3>
        <p>1日の価値観マッチングを通して、下記３点が期待できます。</p>
        <ul>
          <li>自社の魅力の認知向上</li>
          <li>インターンや自社イベントへの集客</li>
          <li>本選考への流入</li>
          <li>参加学生の心にまで踏み込む深く強い訴求が可能</li>
        </ul>
        <h3>参加学生のレベル感（ペルソナ）</h3>
        <p>人材系企業評価値 A:40% B:60</p>
        <ul>
          <li>能力やポテンシャル部分において企業様より評価の高い学生</li>
          <li>価値観という部分に置いて深掘りや将来の像がぼやけている学生</li>
          <li>自分に本気で向き合おうとしている学生</li>
          <li>成長意欲の高い人財</li>
        </ul>
      </div><!-- /.c-alt_container-description -->

      <h3 class="c-alt_container-tagline" data-emergence="hidden">
        イベントのフロー
      </h3>

      <div class="event_flow-wrapper">
		<h4>プレゼンテーション</h4>
        <ul class="event_flow">
          <li><span>メンタリング</span></li>
          <li><span>Finalistプレゼン</span></li>
          <li><span>Finalistマッチング</span></li>
          <li><span>個人プレゼン</span></li>
          <li><span>個人プレゼン者マッチング</span></li>
        </ul>
		<h4>ブース出展</h4>
		 <ul class="event_flow">
          <li><span>ブース観覧</span></li>
	      <li><span>マッチング</span></li>	  
	　　　        </ul>
        <p class="subtext">※考え中カードを掲げている学生はFBを受けて考えを深堀っている学生です<br>
        ※同じ学生に何度も話を聞いて大丈夫です</p>
      </div>
    </div><!-- /.c-alt_container-content -->
  </section>
</article><!-- /.l-content_wrapper -->



<script>
  function initMap() {
    var mapLatlng = new google.maps.LatLng(35.661386,139.714502);

    var mapstyle = [
      {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#444444"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#f2f2f2"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "hue": "#00ff1c"
            },
            {
                "visibility": "on"
            },
            {
                "gamma": "1.69"
            },
            {
                "lightness": "10"
            },
            {
                "saturation": "-28"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 45
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#c0dae4"
            },
            {
                "visibility": "on"
            }
        ]
    },
      {
        featureType:"all",
        elementType:"all",
        stylers: [
          {visibility: "on" }
        ]
      }
    ];

    var myOptions = {
      zoom: 17,
      center: mapLatlng,
      mapTypeIds: [
        'mainmap',
        google.maps.MapTypeId.HYBRID,
        google.maps.MapTypeId.SATELLITE,
        google.maps.MapTypeId.TERRAIN
      ]
    };
    var map = new google.maps.Map(document.getElementById("map"), myOptions);

    var styleMapOptions = {
      map: map,
      name:"アクセス"
    }
    var myMapType = new google.maps.StyledMapType(mapstyle, styleMapOptions);
    map.mapTypes.set('mainmap', myMapType);
    map.setMapTypeId('mainmap');

    var marker = new google.maps.Marker({
        position: mapLatlng
    });

    // To add the marker to the map, call setMap();
    marker.setMap(map)
  }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCxUJRvcDUK4vr6SfvClHfzcZOYnmxPqbw&callback=initMap" async defer></script>
<?php get_template_part('part-page_parallel_links'); ?>

<?php get_footer(); ?>
