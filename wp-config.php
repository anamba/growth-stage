<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.osdn.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.osdn.jp/%E7%94%A8%E8%AA%9E%E9%9B%86#.E3.83.86.E3.82.AD.E3.82.B9.E3.83.88.E3.82.A8.E3.83.87.E3.82.A3.E3.82.BF 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('REVISR_GIT_PATH', ''); // Added by Revisr
define('REVISR_WORK_TREE', '/export/sd216/www/jp/r/e/gmoserver/6/6/sd1003466/growth-stage2018.com/'); // Added by Revisr
define('DB_NAME', 'sddb0040318720');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'sddbMTY1NjY2');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'gw&0XR8S');

/** MySQL のホスト名 */
define('DB_HOST', 'sddb0040318720.cgidb');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'M{TY) rPNUs[|Y$[IVARnFXx+Cs#:x-<&].`*Eyw+RQ=TB,nAp|bTuyO/q2ZtJ $');
define('SECURE_AUTH_KEY',  '$+v&|?6K2BVvH6@Q|=Kh(E9fDZMlnW?.3M{ C$Dgj6iG+#lMA_q9[ig9K0reB)Ld');
define('LOGGED_IN_KEY',    ',&%xCg&^5tpJ_%6~j3Oo}liDl)Q-3|:$&~0|i-[*k}iEO~wxH4U+8qkC-3P.G+Dm');
define('NONCE_KEY',        '?$OtEB6(Ny^i@#c4atx,BCz!efL0e8Ge- >_vzk1IiJqE NjB)4/`$^J8SUEG?C[');
define('AUTH_SALT',        '$~/B&7I8NT)q,+4t60Y~Hb=MRLzNfx9*Rby~ZicdErliT ;mJw+&@Z|U,!rKT-iA');
define('SECURE_AUTH_SALT', 'AGw9jsAZvjo$A~%0[.!zH{|EWs(HN-,sqLIq6m:;nwXn>(IX)`1_~7bHbD>a|c_w');
define('LOGGED_IN_SALT',   'nyX}m&JElQBi:>-_A~x,2Ta[V *ao1;.<e)iExnI~ ZGqgS>`t;NFr_-|j_YPW?v');
define('NONCE_SALT',       'n(,t^wKYn?}op!tS6x!m0Vrr+y)x h|t@nC):UBw{]8|:2@/>++-RSbr/v$ni8 m');


/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
